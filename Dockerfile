# THIS A FMISMANAGER DOCKERFILE FOR RUNNING ON GOOGLE CLOUD
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /app

# Install production dependencies.
# Copy csproj and restore as distinct layers.
COPY FMISManager/*.csproj ./
RUN dotnet restore

# Copy local code to the container image.
COPY ./FMISManager/ ./
WORKDIR /app

# Build a release artifact.
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS runtime
WORKDIR /app
EXPOSE 80
ENV ASPNETCORE_ENVIRONMENT=Production
ENV ASPNETCORE_URLS="http://+:80"
COPY FMISManager/ /app/
COPY --from=build /app/out ./

# Run the service on container startup.
ENTRYPOINT ["dotnet", "FMISManager.dll"]


