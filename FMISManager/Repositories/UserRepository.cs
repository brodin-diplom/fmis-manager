﻿using System.Collections.Generic;
using FMISManager.Models;
using FMISManager.Models.Interface;
using FMISManager.Repositories.Interfaces;
using MongoDB.Driver;

namespace FMISManager.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IMongoCollection<User> _users;

        public UserRepository(IUserDatabaseSettings settings, IDatabaseConnectionSettings dbSettings)
        {
            var client = new MongoClient(dbSettings.ConnectionString);

            var database = client.GetDatabase(settings.DatabaseName);

            _users = database.GetCollection<User>(settings.CollectionName);
        }

        // Create one
        public string Create(User user)
        {
            _users.InsertOne(user);
            return user.Id;
        }

        // Read one
        public User Get(string id) =>
            _users.Find(result => result.Id == id).FirstOrDefault();

        // Read all
        public List<User> Get() =>
            _users.Find(user => true).ToList();

        // Update one
        public void Update(string id, User userIn) =>
            _users.ReplaceOne(user => user.Id == id, userIn);

        // Delete with id
        public void Delete(string id) =>
            _users.DeleteOne(user => user.Id == id);

        // Delete with object
        public void Delete(User userIn) =>
            _users.DeleteOne(user => user.Id == userIn.Id);
    }
}
