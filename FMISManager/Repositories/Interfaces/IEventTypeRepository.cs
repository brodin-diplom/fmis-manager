﻿using System.Collections.Generic;
using FMISManager.Models;

namespace FMISManager.Repositories.Interfaces
{
    public interface IEventTypeRepository
    {
        // Create one
        string Create(EventType eventType);

        // Read one
        EventType Get(string id);

        // Read all
        List<EventType> Get();

        // Delete with id
        void Delete(string id);
    }
}
