﻿using System.Collections.Generic;
using FMISManager.Models;

namespace FMISManager.Repositories.Interfaces
{
    public interface IUserRepository
    {
        // Create one
        string Create(User user);

        // Read one
        User Get(string id);

        // Read all
        List<User> Get();

        // Update one
        void Update(string id, User user);

        // Delete with id
        void Delete(string id);

        // Delete with object
        void Delete(User result);
    }
}
