﻿using System.Collections.Generic;
using FMISManager.Models;

namespace FMISManager.Repositories.Interfaces
{
    public interface ISimulationsRepository
    {
        // Create one
        string Create(Simulation simulation);

        // Read one
        Simulation Get(string id);

        // Read all
        List<Simulation> GetAllForUser(string uid);

        // Update one
        void Update(string id, Simulation simulation);
        
        // Delete with id
        void Delete(string id);

        // Delete with object
        void Delete(Simulation simulation);
    }
}
