﻿using System.Collections.Generic;
using FMISManager.Models;
using Newtonsoft.Json.Linq;

namespace FMISManager.Repositories.Interfaces
{
    public interface IFieldRepository
    {
        // Create one
        string Create(Field field);

        // Read one
        Field Get(string id);

        // Read all
        List<Field> GetAllForUser(string uid);

        // Read field zones
        List<Zone> GetFieldZones(string id);

        // Read field zone
        Zone GetFieldZone(string fieldId, string id);

        // Update one
        void Update(string id, Field field);
        
        // Delete with id
        void Delete(string id);

        // Delete with object
        void Delete(Field field);

        // Update Zone with defaction
        void UpdateZoneWithDefaction(string fieldId, Zone zoneOld, JObject body);

        // Update Zone defaction with event
        void UpdateZoneDefactionWithEvent(string fieldId, Zone zoneOld, string defactionId, JObject body);
    }
}
