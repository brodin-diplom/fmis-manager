﻿using System.Collections.Generic;
using FMISManager.Models;
using FMISManager.Models.Interface;
using FMISManager.Repositories.Interfaces;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace FMISManager.Repositories
{
    public class EventTypeRepository : IEventTypeRepository
    {
        private readonly IMongoCollection<EventType> _eventTypes;

        public EventTypeRepository(IEventTypeDatabaseSettings settings, IDatabaseConnectionSettings dbSettings)
        {
            var client = new MongoClient(dbSettings.ConnectionString);

            var database = client.GetDatabase(settings.DatabaseName);

            _eventTypes = database.GetCollection<EventType>(settings.CollectionName);
        }

        // Create one
        public string Create(EventType eventType)
        {
            _eventTypes.InsertOne(eventType);
            return eventType.Id.ToString();
        }

        // Read one
        public EventType Get(string id) =>
            _eventTypes.Find(eventType => eventType.Id == id).FirstOrDefault();

        // Read all
        public List<EventType> Get() =>
            _eventTypes.Find(eventType => true).ToList();

        // Delete with id
        public void Delete(string id) =>
            _eventTypes.DeleteOne(user => user.Id == id);
    }
}
