﻿using System.Collections.Generic;
using FMISManager.Models;
using FMISManager.Models.Interface;
using FMISManager.Repositories.Interfaces;
using MongoDB.Driver;

namespace FMISManager.Repositories
{
    public class SimulationsRepository : ISimulationsRepository
    {
        private readonly IMongoCollection<Simulation> _simulations;

        public SimulationsRepository(ISimulationsDatabaseSettings settings, IDatabaseConnectionSettings dbSettings)
        {
            var client = new MongoClient(dbSettings.ConnectionString);

            var database = client.GetDatabase(settings.DatabaseName);

            _simulations = database.GetCollection<Simulation>(settings.CollectionName);
        }

        // Create one
        public string Create(Simulation simulation)
        {
            _simulations.InsertOne(simulation);
            return simulation.Id;
        }

        // Read one
        public Simulation Get(string id) =>
            _simulations.Find(result => result.Id == id).FirstOrDefault();

        // Read all
        public List<Simulation> GetAllForUser(string uid) =>
            _simulations.Find(field => field.Uid == uid).ToList();

        // Update one
        public void Update(string id, Simulation simulationIn) =>
            _simulations.ReplaceOne(simulation => simulation.Id == id, simulationIn);

        // Delete with id
        public void Delete(string id) =>
            _simulations.DeleteOne(simulation => simulation.Id == id);

        // Delete with object
        public void Delete(Simulation simulationIn) =>
            _simulations.DeleteOne(simulation => simulation.Id == simulationIn.Id);
    }
}
