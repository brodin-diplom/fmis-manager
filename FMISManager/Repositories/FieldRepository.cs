﻿using System;
using System.Collections.Generic;
using FMISManager.Models;
using FMISManager.Models.Interface;
using FMISManager.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using Newtonsoft.Json.Linq;

namespace FMISManager.Repositories
{
    public class FieldRepository : IFieldRepository
    {
        private readonly IMongoCollection<Field> _fields;

        public FieldRepository(IFieldDatabaseSettings settings, IDatabaseConnectionSettings dbSettings)
        {
            var client = new MongoClient(dbSettings.ConnectionString);

            var database = client.GetDatabase(settings.DatabaseName);

            _fields = database.GetCollection<Field>(settings.CollectionName);
        }

        // Create one
        public string Create(Field field)
        {
            _fields.InsertOne(field);
            return field.Id;
        }

   
        // Read one
        public Field Get(string id) =>
            _fields.Find(field => field.Id == id).FirstOrDefault();

        // Read all
        public List<Field> GetAllForUser(string uid) =>
            _fields.Find(field => field.Uid == uid).ToList();

        // Read field zones
        public List<Zone> GetFieldZones(string id) =>
            _fields.Find(field => field.Id == id).FirstOrDefault().Zones;

        // Read field single zone
        public Zone GetFieldZone(string fieldId, string id)
        {
            var zones = _fields.Find(field => field.Id == fieldId).FirstOrDefault().Zones;
            return zones.Find(zone => zone.Id == id);
        }
           
        // Update one
        public void Update(string id, Field fieldIn) =>
            _fields.ReplaceOne(field => field.Id == id, fieldIn);

        // Update zone with defaction
        public void UpdateZoneWithDefaction(string fieldId, Zone zoneOld, JObject body)
        {
            Defaction defaction = new();
            defaction.Id = Guid.NewGuid().ToString();
            defaction.Name = body["name"].ToString();

            List<Event> events = new List<Event>();
            Event defactionEvent = new();
            defactionEvent.Id = Guid.NewGuid().ToString();
            defactionEvent.Type = body["event"]["type"].ToString();
            defactionEvent.CropType = body["event"]["cropType"].ToString();
            defactionEvent.FertilizationType = body["event"]["fertilizationType"].ToString();
            defactionEvent.NitrogenLevel = int.Parse(body["event"]["nitrogenLevel"].ToString());
            defactionEvent.Date = DateTime.Parse(body["event"]["date"].ToString());
            events.Add(defactionEvent);

            defaction.Events = events;

            Field field = Get(fieldId);
            foreach(Zone zone in field.Zones)
            {
                if(zone.Id == zoneOld.Id)
                {
                    zone.Defactions.Add(defaction);
                }
            }

            _fields.ReplaceOne(field => field.Id == fieldId, field);
        }

        // Update zone defaction
        public void UpdateZoneDefactionWithEvent(string fieldId, Zone zoneOld, string defactionId, JObject body)
        {
            Field field = Get(fieldId);

            Event defactionEvent = new();
            defactionEvent.Id = Guid.NewGuid().ToString();
            defactionEvent.Type = body["type"].ToString();
            defactionEvent.CropType = body["cropType"].ToString();
            defactionEvent.FertilizationType = body["fertilizationType"].ToString();
            defactionEvent.NitrogenLevel = int.Parse(body["nitrogenLevel"].ToString());
            defactionEvent.Date = DateTime.Parse(body["date"].ToString());

            foreach (Zone zone in field.Zones)
            {
                if(zone.Id == zoneOld.Id)
                {
                    foreach(Defaction defaction in zone.Defactions)
                    {
                        if(defaction.Id == defactionId)
                        {
                            defaction.Events.Add(defactionEvent);
                        }
                    }
                }
                
            }

            _fields.ReplaceOne(field => field.Id == fieldId, field);
            
        }

        // Delete with id
        public void Delete(string id) =>
            _fields.DeleteOne(field => field.Id == id);

        // Delete with object
        public void Delete(Field fieldIn) =>
            _fields.DeleteOne(field => field.Id == fieldIn.Id);
    }
}
