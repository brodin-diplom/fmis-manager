﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace FMISManager.Models
{
    [BsonIgnoreExtraElements]
    public class Simulation
    {
        [BsonId]
        [BsonElement("Id")]
        [BsonRepresentation(BsonType.ObjectId)]
        [JsonProperty("id")]
        public string Id { get; set; }

        [BsonElement("FieldId")]
        [JsonProperty("field_id")]
        public string FieldId { get; set; }

        [BsonElement("Uid")]
        [JsonProperty("uid")]
        public string Uid { get; set; }

        [BsonElement("Name")]
        [JsonProperty("name")]
        public string Name { get; set; }

        [BsonElement("Simulations")]
        [JsonProperty("simulations")]
        public List<ZoneSimResults> Simulations { get; set; }

    }
}