﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace FMISManager.Models
{
    [BsonIgnoreExtraElements]
    public class ZoneSimResults
    {
        [BsonId]
        [BsonElement("ZoneId")]
        [JsonProperty("zone_id")]
        public string ZoneId { get; set; }

        [BsonElement("Results")]
        [JsonProperty("results")]
        public List<HarvestResult> Results { get; set; }
    }
}