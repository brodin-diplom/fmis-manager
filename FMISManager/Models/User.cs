﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace FMISManager.Models
{
    [BsonIgnoreExtraElements]
    public class User
    {
        [BsonId]
        [BsonElement("Id")]
        [JsonProperty("id")]
        public string Id { get; set; }

        [BsonElement("Email")]
        [JsonProperty("email")]
        public string Email { get; set; }

    }
}
