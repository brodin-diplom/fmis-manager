﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace FMISManager.Models
{
    public class HarvestResult
    {
        [BsonElement("Year")]
        [JsonProperty("year")]
        public long Year { get; set; }

        [BsonElement("Month")]
        [JsonProperty("month")]
        public long Month { get; set; }

        [BsonElement("Day")]
        [JsonProperty("day")]
        public long Day { get; set; }

        [BsonElement("Column")]
        [JsonProperty("column")]
        public string Column { get; set; }

        [BsonElement("Crop")]
        [JsonProperty("crop")]
        public string Crop { get; set; }

        [BsonElement("Stem_DM")]
        [JsonProperty("stem_DM")]
        public double StemDm { get; set; }

        [BsonElement("Dead_DM")]
        [JsonProperty("dead_DM")]
        public double DeadDm { get; set; }

        [BsonElement("Leaf_DM")]
        [JsonProperty("leaf_DM")]
        public double LeafDm { get; set; }

        [BsonElement("Sorg_DM")]
        [JsonProperty("sorg_DM")]
        public double SorgDm { get; set; }

        [BsonElement("Stem_N")]
        [JsonProperty("stem_N")]
        public double StemN { get; set; }

        [BsonElement("Dead_N")]
        [JsonProperty("dead_N")]
        public double DeadN { get; set; }

        [BsonElement("Leaf_N")]
        [JsonProperty("leaf_N")]
        public double LeafN { get; set; }

        [BsonElement("Sorg_N")]
        [JsonProperty("sorg_N")]
        public double SorgN { get; set; }

        [BsonElement("WStress")]
        [JsonProperty("WStress")]
        public long WStress { get; set; }

        [BsonElement("NStress")]
        [JsonProperty("NStress")]
        public double NStress { get; set; }

        [BsonElement("WP_ET")]
        [JsonProperty("WP_ET")]
        public double WpEt { get; set; }

        [BsonElement("HI")]
        [JsonProperty("HI")]
        public double Hi { get; set; }
    }
}
