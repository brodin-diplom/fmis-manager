﻿namespace FMISManager.Models.Interface
{
    public interface IFieldDatabaseSettings
    {
        string CollectionName { get; set; }
        string DatabaseName { get; set; }
    }

    public interface IUserDatabaseSettings
    {
        string CollectionName { get; set; }
        string DatabaseName { get; set; }
    }

    public interface ISimulationsDatabaseSettings
    {
        string CollectionName { get; set; }
        string DatabaseName { get; set; }
    }

    public interface IEventTypeDatabaseSettings
    {
        string CollectionName { get; set; }
        string DatabaseName { get; set; }
    }

    public interface IDatabaseConnectionSettings
    {
        string ConnectionString { get; set; }
    }
}
