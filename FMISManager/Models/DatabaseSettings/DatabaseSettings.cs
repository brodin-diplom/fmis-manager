﻿using FMISManager.Models.Interface;

namespace FMISManager.Models
{
    public class FieldDatabaseSettings : IFieldDatabaseSettings
    {
        public string CollectionName { get; set; }
        public string DatabaseName { get; set; }
    }
    

    public class UserDatabaseSettings : IUserDatabaseSettings
    {
        public string CollectionName { get; set; }
        public string DatabaseName { get; set; }
    }

    public class SimulationsDatabaseSettings : ISimulationsDatabaseSettings
    {
        public string CollectionName { get; set; }
        public string DatabaseName { get; set; }
    }

    public class EventTypeDatabaseSettings : IEventTypeDatabaseSettings
    {
        public string CollectionName { get; set; }
        public string DatabaseName { get; set; }
    }

    public class DatabaseConnectionSettings : IDatabaseConnectionSettings
    {
        public string ConnectionString { get; set; }
    }
}
