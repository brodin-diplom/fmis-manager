﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace FMISManager.Models
{
    [BsonIgnoreExtraElements]
    public class Event
    {
        [BsonId]
        [JsonProperty("id")]
        public string Id { get; set; }

        [BsonElement("Type")]
        [JsonProperty("type")]
        public string Type { get; set; }

        [BsonElement("FertilizationType")]
        [JsonProperty("fertilization_type")]
        public string FertilizationType { get; set; }

        [BsonElement("CropType")]
        [JsonProperty("crop_type")]
        public string CropType { get; set; }

        [BsonElement("NitrogenLevel")]
        [JsonProperty("nitrogen_level")]
        public int NitrogenLevel { get; set; }

        [BsonElement("Date")]
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        
    }
}