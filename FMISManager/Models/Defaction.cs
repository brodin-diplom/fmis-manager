﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace FMISManager.Models
{
    public class Defaction
    {
        [BsonId]
        [JsonProperty("id")]
        public string Id { get; set; }

        [BsonElement("Name")]
        [JsonProperty("name")]
        public string Name { get; set; }

        [BsonElement("Events")]
        [JsonProperty("events")]
        public List<Event> Events { get; set; }

    }
}
