﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace FMISManager.Models
{
    [BsonIgnoreExtraElements]
    public class EventType
    {
        [BsonId]
        [BsonElement("Id")]
        [JsonProperty("id")]
        public string Id { get; set; }

        [BsonElement("Name")]
        [JsonProperty("name")]
        public string Name { get; set; }

        [BsonElement("EventTypes")]
        [JsonProperty("eventTypes")]
        public List<string> EventTypes { get; set; }

    }
}
