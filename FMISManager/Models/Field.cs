﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace FMISManager.Models
{
    [BsonIgnoreExtraElements]
    public class Field
    {
        [BsonId]
        [JsonProperty("id")]
        public string Id { get; set; }

        [BsonElement("Name")]
        [JsonProperty("name")]
        public string Name { get; set; }

        [BsonElement("KmlUrl")]
        [JsonProperty("kmlUrl")]
        public string KmlUrl { get; set; }

        [BsonElement("Zones")]
        [JsonProperty("zones")]
        public List<Zone> Zones { get; set; }

        [BsonElement("Uid")]
        [JsonProperty("uid")]
        public string Uid { get; set; }
    }
}
