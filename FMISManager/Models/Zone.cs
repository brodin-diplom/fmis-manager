﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace FMISManager.Models
{
    [BsonIgnoreExtraElements]
    public class Zone
    {
        [BsonId]
        [JsonProperty("id")]
        public string Id { get; set; }

        [BsonElement("Name")]
        [JsonProperty("name")]
        public string Name { get; set; }

        [BsonElement("Defactions")]
        [JsonProperty("defactions")]
        public List<Defaction> Defactions { get; set; }
    }
}
