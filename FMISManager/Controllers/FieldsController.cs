﻿using ChoETL;
using FMISManager.Models;
using FMISManager.Repositories.Interfaces;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FMISManager.Controllers
{
    [EnableCors("OriginPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class FieldsController : ControllerBase
    {
        private readonly IFieldRepository _fieldRepository;
        private readonly ISimulationsRepository _simulationsRepository;
        private HttpClient _httpClient;

        public FieldsController(IFieldRepository fieldRepository, ISimulationsRepository simulationsRepository, HttpClient httpClient)
        {
            _fieldRepository = fieldRepository;
            _simulationsRepository = simulationsRepository;
            _httpClient = httpClient;
        }

        /// <summary>
        /// Get all fields from a user
        /// </summary>
        /// <remarks>
        /// The uid is the id of a user, and the userid from firebase
        /// </remarks>
        /// <param name="uid"></param>
        /// <returns>List of fields</returns>
        [HttpGet]
        public ActionResult<List<Field>> GetAllForUser([FromQuery] string uid) =>
            _fieldRepository.GetAllForUser(uid);

        /// <summary>
        /// Get all zones from a field
        /// </summary>
        /// <remarks>
        /// The id is the id of a field
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>List of zones</returns>
        [HttpGet]
        [Route("{id}/zones")]
        public ActionResult<List<Zone>> GetFieldZones(string id) =>
            _fieldRepository.GetFieldZones(id);

        /// <summary>
        /// Get one zone from a field
        /// </summary>
        /// <remarks>
        /// The fieldId is the id of a field, the id is the id of a zone
        /// </remarks>
        /// <param name="fieldId"></param>
        /// <param name="id"></param>
        /// <returns>The zone</returns>
        [HttpGet]
        [Route("{fieldId}/zones/{id}")]
        public ActionResult<Zone> GetFieldZone(string fieldId, string id) =>
            _fieldRepository.GetFieldZone(fieldId, id);

        /// <summary>
        /// Get one field
        /// </summary>
        /// <remarks>
        /// The id is the id of a field
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>The zone</returns>
        [HttpGet("{id}", Name = "GetField")]
        public ActionResult<Field> Get(string id)
        {
            var result = _fieldRepository.Get(id);

            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        /// <summary>
        /// Get daisy zonefile of a zone
        /// </summary>
        /// <param name="fieldId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{fieldId}/zones/{id}/get-zone-file")]
        public async Task<ActionResult<Zone>> GetFieldZoneFile(string fieldId, string id)
        {
            string converterUrl = "https://fmis-converter-hjnkolnyrq-oa.a.run.app/api/DaisyConverter/from-json/to-management-file";

            try
            {
                var field = _fieldRepository.Get(fieldId);
                var zone = _fieldRepository.GetFieldZone(fieldId, id);

                // The files for daisyWrapper
                List<(Stream, string)> streamTuples = new();

                var zoneObject = JObject.Parse(JsonConvert.SerializeObject(zone));
                zoneObject["field_name"] = field.Name;
                zoneObject["name"] = "standard"; // TODO: An improvement is implementing the soil generating aswell

                StringContent httpContent = new StringContent(zoneObject.ToString(), Encoding.UTF8, "application/json");
                var converterResponse = await _httpClient.PostAsync($"{converterUrl}?fileName=Zone_{zone.Name}.dai", httpContent);
                converterResponse.EnsureSuccessStatusCode();

                // Convert response into filestream
                MemoryStream stream = new(await converterResponse.Content.ReadAsByteArrayAsync());
                var fileName = $"Zone_{zone.Name}.dai";

                return File(stream, contentType: "application/octet-stream", fileDownloadName: fileName);
            }
            catch
            {
                return StatusCode(500, $"Internal server error");
            }
        }

        /// <summary>
        /// Create one field
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /fields
        ///     {
        ///         "kmlUrl": "https://kmlURL.com",
        ///         "name": "Test kml upload",
        ///         "uid": "rMVaPTv98qfhKKR0kKJp3qG9ECz2"
        ///     }
        /// </remarks>
        /// <param name="body"></param>
        /// <returns>The field</returns>
        [HttpPost]
        public ActionResult<Field> Create([FromBody] JObject body)
        {
            using (var client = new WebClient())
            {
                client.DownloadFile(body["kmlUrl"].ToString(), "kml.kml");
            }

            List<Zone> zones = new();
            XmlDocument xml = new XmlDocument();
            xml.Load("kml.kml");

            XmlNodeList listOfPlacemarks = xml.GetElementsByTagName("Placemark");
            foreach (XmlElement node in listOfPlacemarks)
            {
                var names = node.GetElementsByTagName("name");

                foreach (XmlElement name in names)
                {
                    Zone newZone = new();
                    newZone.Id = name.FirstChild.Value;
                    newZone.Name = name.FirstChild.Value;
                    newZone.Defactions = new List<Defaction>();

                    var alreadyExist = false;

                    foreach (Zone zone in zones.ToList())
                    {
                        if (zone.Id == newZone.Id)
                        {
                            alreadyExist = true;
                        }
                    }

                    if (!alreadyExist)
                    {
                        zones.Add(newZone);
                    }
                }
            }

            Field field = new();
            field.KmlUrl = body["kmlUrl"].ToString();
            field.Zones = zones;
            field.Name = body["name"].ToString();
            field.Uid = body["uid"].ToString();
            field.Id = Guid.NewGuid().ToString();

            _fieldRepository.Create(field);

            return CreatedAtRoute("GetField", new { id = field.Id.ToString() }, field);

        }

        /// <summary>
        /// Update one field
        /// </summary>
        /// <remarks>
        ///  The id is the id of a field, the fieldIn is the field in json see object definition in the bottom of the page
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="fieldIn"></param>
        /// <returns>Statuscode 200</returns>
        [HttpPut("{id}")]
        public IActionResult Update(string id, Field fieldIn)
        {
            var result = _fieldRepository.Get(id);

            if (result == null)
            {
                return NotFound();
            }

            _fieldRepository.Update(id, fieldIn);

            return Ok();
        }

        /// <summary>
        /// Update a zone with defaction
        /// </summary>
        /// <param name="fieldId"></param>
        /// <param name="id"></param>
        /// <param name="request"></param>
        [HttpPut]
        [Route("{fieldId}/zones/{id}")]
        public ActionResult<Zone> UpdateZoneWithDefaction(string fieldId, string id, [FromBody] JObject request)
        {
            var result = _fieldRepository.GetFieldZone(fieldId, id);

            if (result == null)
            {
                return NotFound();
            }

            _fieldRepository.UpdateZoneWithDefaction(fieldId, result, request);

            return NoContent();
        }

        /// <summary>
        /// Update defaction in a zone
        /// </summary>
        /// <param name="fieldId"></param>
        /// <param name="zoneId"></param>
        /// <param name="defactionId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{fieldId}/zones/{zoneId}/defactions/{defactionId}")]
        public ActionResult<Zone> UpdateZoneDefaction(string fieldId, string zoneId, string defactionId, [FromBody] JObject request)
        {
            var result = _fieldRepository.GetFieldZone(fieldId, zoneId);

            if (result == null)
            {
                return NotFound();
            }
            else
            {
                _fieldRepository.UpdateZoneDefactionWithEvent(fieldId, result, defactionId, request);
            }


            return NoContent();
        }

        /// <summary>
        /// Delete a zone
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var result = _fieldRepository.Get(id);

            if (result == null)
            {
                return NotFound();
            }

            _fieldRepository.Delete(result.Id);

            return NoContent();
        }

        /// <summary>
        /// Start a simulation with the daisy wrapper from a selected field
        /// </summary>
        /// <param name="body"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <remarks>
        /// Can only generate a management file from json
        /// Sample request:
        ///
        ///     POST /{id}/runSimulation (id ex: cf8f4c1e-763a-4692-802b-e4260dee69e5)
        ///     {
        ///         "nitrogenValues": [
        ///             {
        ///                 "zoneId" : "1",
        ///                 "nValue" : 0
        ///             }
        ///         ]
        ///     }
        /// </remarks>
        /// <returns>A json represented array with harvest objects</returns>
        /// <response code="200">Returns the newly simulated zone</response>
        /// <response code="500">If any other error occured</response>
        [HttpPost]
        [Route("{id}/runSimulation")]
        public async Task<IActionResult> RunSimulation([FromBody] JObject body, string id)
        {
            string converterUrl = "https://fmis-converter-hjnkolnyrq-oa.a.run.app/api/DaisyConverter/from-json/to-management-file";
            string daisyUrl = "http://34.88.179.59/daisywrapper/simulate";

            
            try
            {
            
                var fieldToSim = _fieldRepository.Get(id);
                List<Zone> zonesToRunSim = new();

                // Go through zones in the field, and if there is fertilize, add them to the simulation stack
                foreach (Zone zone in fieldToSim.Zones)
                {
                    foreach (Defaction defaction in zone.Defactions)
                    {
                        if (zone.Defactions.Count() != 0)
                        {
                            foreach (Event zoneEvent in defaction.Events)
                            {
                                if (zoneEvent.Type == "fertilize")
                                {
                                    zonesToRunSim.Add(zone);
                                }
                            }
                        }
                    }
                }

                // Change the nitrogen value of the simulation zones
                foreach (Zone zone in zonesToRunSim)
                {
                    foreach (Defaction defaction in zone.Defactions)
                    {
                        foreach (Event e in defaction.Events)
                        {
                            foreach (var v in body["nitrogenValues"])
                            {
                                if (v["zoneId"].ToString() == zone.Id)
                                {
                                    e.NitrogenLevel = int.Parse(v["nValue"].ToString());
                                }
                            }

                        }
                    }
                }

                // The files for daisyWrapper
                List<(Stream, string)> streamTuples = new();

                // Convert the zone object to .dai files, and load them into memory
                // Using the fmis converter api
                foreach (Zone zone in zonesToRunSim)
                {
                    var zoneObject = JObject.Parse(JsonConvert.SerializeObject(zone));

                    zoneObject["field_name"] = fieldToSim.Name;
                    zoneObject["name"] = "standard"; // TODO: An improvement is implementing the soil generating aswell

                    StringContent httpContent = new StringContent(zoneObject.ToString(), Encoding.UTF8, "application/json");
                    var converterResponse = await _httpClient.PostAsync($"{converterUrl}?fileName=Zone_{zone.Name}.dai", httpContent);
                    MemoryStream stream = new MemoryStream(await converterResponse.Content.ReadAsByteArrayAsync());
                    streamTuples.Add((stream, $"Zone_{zone.Name}.dai"));
                    converterResponse.EnsureSuccessStatusCode();
                }

                // Load the standard files to push into memory aswell
                foreach (var fileInfo in new DirectoryInfo("SimFiles").GetFiles())
                {
                    streamTuples.Add((new FileStream(fileInfo.FullName, FileMode.Open), fileInfo.Name));
                }


                // The formDataContent for daisy wrapper
                var daisyRunContent = new MultipartFormDataContent();

                // Add the necessary headers for the daisy wrapper
                foreach (var tuple in streamTuples)
                {
                    daisyRunContent.Add(CreateFileContent(tuple.Item1, tuple.Item2, "application/octet-stream"));
                }

                // Call the daisywrapper
                var daisyResponse = await _httpClient.PostAsync(daisyUrl, daisyRunContent);
                daisyResponse.EnsureSuccessStatusCode();

                List<ZoneSimResults> zoneResults = new();

                // Open the zip recieved by daisy wrapper into memory
                using (MemoryStream stream = new MemoryStream(await daisyResponse.Content.ReadAsByteArrayAsync()))
                {
                    // Open the zip recieved by daisy wrapper into memory
                    using (var zip = new ZipArchive(stream, ZipArchiveMode.Read))
                    {
                        StreamReader reader;

                        int i = 0;
                        foreach (var entry in zip.Entries)
                        {
                            // Open the stream
                            using var fileStream = entry.Open();

                            // Read the file
                            reader = new(fileStream, Encoding.UTF8);
                            var streamText = reader.ReadToEnd();

                            // If the file is a harvest file, parse the file in memory
                            if (entry.Name == "harvest.csv")
                            {
                                Console.WriteLine($"Harvest file recieved: {streamText}");

                                // Fixed number of lines until header reached in a harvest file
                                int headerLineForHarvest = 11;

                                StringBuilder sb = new();

                                // Convert the .csv file to json with the ChoETL library
                                using (var p = ChoCSVReader.LoadText(streamText)
                                    .WithHeaderLineAt(headerLineForHarvest)
                                    .WithDelimiter("\t"))
                                {
                                    using var w = new ChoJSONWriter(sb);
                                    w.Write(p);
                                    w.Dispose();
                                }

                                Console.WriteLine($"Harvest file converted: {sb}");

                                // Convert the json to a JArray, to remove first entry, which is just units
                                var array = JArray.Parse(sb.ToString());

                                // Remove units
                                array.RemoveAt(0);

                                // Convert the array into the harvestrestult list
                                var harvestResults = JsonConvert.DeserializeObject<List<HarvestResult>>(array.ToString());

                                // Retrieve the zone id from the folder name

                                // Add the results to the zone result list
                                zoneResults.Add(new ZoneSimResults()
                                {
                                    ZoneId = $"{entry.FullName.Split("/")[0].Split("_")[1]}",
                                    Results = harvestResults
                                });
                                i++;
                            }
                            else
                            {
                                Console.WriteLine($"Other file recieved, will not be saved:\n {streamText}");
                            }
                            fileStream.Dispose();
                            reader.Dispose();
                        }

                        // Dispose all the streams
                        foreach (var tuple in streamTuples)
                        {
                            tuple.Item1.Dispose();
                        }
                        stream.Dispose();

                        // If no zone results were found, throw error
                        if (zoneResults.Count == 0)
                        {
                            return BadRequest("The harvest file from simulation was not found");
                        }

                        // Create new simulation object with the zoneResults in the database
                        _simulationsRepository.Create(new Simulation()
                        {
                            FieldId = fieldToSim.Id,
                            Name = fieldToSim.Name,
                            Uid = fieldToSim.Uid,
                            Simulations = zoneResults
                        });

                    var jsonObject = new JObject
                    {
                        { "results", JArray.Parse(JsonConvert.SerializeObject(zoneResults)) }
                    };

                    return Ok(jsonObject);
                    }
                }
                
            }
            catch (Exception e)
            {
                return StatusCode(500, $"Internal server error: {e.Message}");
            }
                

        }

        /*/
         * Taken from 
         * https://stackoverflow.com/questions/16906711/httpclient-how-to-upload-multiple-files-at-once
         */
        private StreamContent CreateFileContent(Stream stream, string fileName, string contentType)
        {
            var fileContent = new StreamContent(stream);
            fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
            {
                Name = "\"files\"",
                FileName = "\"" + fileName + "\""
            }; // the extra quotes are key here
            fileContent.Headers.ContentType = new MediaTypeHeaderValue(contentType);
            return fileContent;
        }
    }
}
