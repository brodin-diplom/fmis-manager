﻿using FMISManager.Models;
using FMISManager.Repositories.Interfaces;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace FMISManager.Controllers
{
    [EnableCors("OriginPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _userRepository;

        public UsersController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<List<User>> Get() =>
            _userRepository.Get();

        /// <summary>
        /// Get one user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetUser")]
        public ActionResult<User> Get(string id)
        {
            var result = _userRepository.Get(id);

            if (result == null)
            {
                return NotFound();
            }

            return result;
        }

        /// <summary>
        /// Create a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<User> Create(User user)
        {
            _userRepository.Create(user);

            return CreatedAtRoute("GetUser", new { id = user.Id.ToString() }, user);
        }

        /// <summary>
        /// Update a user
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userIn"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Update(string id, User userIn)
        {
            var result = _userRepository.Get(id);

            if (result == null)
            {
                return NotFound();
            }

            _userRepository.Update(id, userIn);

            return NoContent();
        }

        /// <summary>
        /// Delete a user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var result = _userRepository.Get(id);

            if (result == null)
            {
                return NotFound();
            }

            _userRepository.Delete(result.Id);

            return NoContent();
        }
    }
}
