﻿using FMISManager.Models;
using FMISManager.Repositories.Interfaces;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace FMISManager.Controllers
{
    [EnableCors("OriginPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class SimulationsController : ControllerBase
    {
        private readonly ISimulationsRepository _simulationsRepository;

        public SimulationsController(ISimulationsRepository simulationsRepository)
        {
            _simulationsRepository = simulationsRepository;
        }


        /// <summary>
        /// Get all simulations for a user
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<List<Simulation>> GetAllForUser([FromQuery] string uid) =>
            _simulationsRepository.GetAllForUser(uid);

        /// <summary>
        /// Get one simulation
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetSimulation")]
        public ActionResult<Simulation> Get(string id)
        {
            var result = _simulationsRepository.Get(id);

            if (result == null)
            {
                return NotFound();
            }

            return result;
        }

        /// <summary>
        /// Create one simulation
        /// </summary>
        /// <param name="simulation"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<Simulation> Create(Simulation simulation)
        {
            _simulationsRepository.Create(simulation);

            return Ok("");
        }

        /// <summary>
        /// Update a simulation
        /// </summary>
        /// <param name="id"></param>
        /// <param name="simulationIn"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Update(string id, Simulation simulationIn)
        {
            var result = _simulationsRepository.Get(id);

            if (result == null)
            {
                return NotFound();
            }

            _simulationsRepository.Update(id, simulationIn);

            return NoContent();
        }

        /// <summary>
        /// Delete a simulation
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var result = _simulationsRepository.Get(id);

            if (result == null)
            {
                return NotFound();
            }

            _simulationsRepository.Delete(result.Id);

            return NoContent();
        }

    }
}
