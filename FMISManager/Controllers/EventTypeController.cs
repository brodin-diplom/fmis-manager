﻿using FMISManager.Models;
using FMISManager.Repositories.Interfaces;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace FMISManager.Controllers
{
    [EnableCors("OriginPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class EventTypeController : ControllerBase
    {
        private readonly IEventTypeRepository _eventTypeRepository;

        public EventTypeController(IEventTypeRepository eventTypeRepository)
        {
            _eventTypeRepository = eventTypeRepository;
        }

        /// <summary>
        /// Get all EventType
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<List<EventType>> Get() =>
            _eventTypeRepository.Get();

        /// <summary>
        /// Post a new EventType
        /// </summary>
        /// <param name="eventType"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<EventType> Create(EventType eventType)
        {
            _eventTypeRepository.Create(eventType);
            return Ok("Done");
        }


        /// <summary>
        /// Get one EventType
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetEventType")]
        public ActionResult<EventType> Get(string id)
        {
            var eventType = _eventTypeRepository.Get(id);

            if (eventType == null)
            {
                return NotFound();
            }

            return eventType;
        }

        /// <summary>
        /// Delete an EventType
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var eventType = _eventTypeRepository.Get(id);

            if (eventType == null)
            {
                return NotFound();
            }

            _eventTypeRepository.Delete(eventType.Id);

            return NoContent();
        }
    }

}
