﻿using System;
using System.IO;
using System.Net.Http;
using System.Reflection;
using FMISManager.Models;
using FMISManager.Models.Interface;
using FMISManager.Repositories;
using FMISManager.Repositories.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;

namespace FMISManager
{
    public class Startup
    {
        private readonly string swaggerName = "FMIS Manager";
        private readonly string swaggerVersion = "v1";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("OriginPolicy", builder =>
                {
                    builder.AllowAnyOrigin()
                           .AllowAnyMethod()
                           .AllowAnyHeader();
                });
            });

            services.AddSingleton(new HttpClient());

            // Gather default settings from app-settings
            services.Configure<FieldDatabaseSettings>(
                Configuration.GetSection(nameof(FieldDatabaseSettings)));

            services.Configure<UserDatabaseSettings>(
                Configuration.GetSection(nameof(UserDatabaseSettings)));

            services.Configure<SimulationsDatabaseSettings>(
                Configuration.GetSection(nameof(SimulationsDatabaseSettings)));

            services.Configure<EventTypeDatabaseSettings>(
                Configuration.GetSection(nameof(EventTypeDatabaseSettings)));

            services.Configure<DatabaseConnectionSettings>(
                Configuration.GetSection(nameof(DatabaseConnectionSettings)));

            // Add singletons for settings
            services.AddSingleton<IFieldDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<FieldDatabaseSettings>>().Value);

            services.AddSingleton<IUserDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<UserDatabaseSettings>>().Value);

            services.AddSingleton<ISimulationsDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<SimulationsDatabaseSettings>>().Value);

            services.AddSingleton<IEventTypeDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<EventTypeDatabaseSettings>>().Value);

            services.AddSingleton<IDatabaseConnectionSettings>(sp =>
                sp.GetRequiredService<IOptions<DatabaseConnectionSettings>>().Value);

            // Add repository singletons
            services.AddScoped(typeof(IFieldRepository), typeof(FieldRepository));

            services.AddScoped(typeof(IUserRepository), typeof(UserRepository));

            services.AddScoped(typeof(ISimulationsRepository), typeof(SimulationsRepository));

            services.AddScoped(typeof(IEventTypeRepository), typeof(EventTypeRepository));

            services.AddControllers().AddNewtonsoftJson(options => options.UseMemberCasing());

            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(swaggerVersion, new OpenApiInfo
                {
                    Title = swaggerName,
                    Version = swaggerVersion,
                    Description = $"This '{swaggerName}' service was made by " +
                    "Niklaes Jacobsen (s160198) & " +
                    "Sebastian Sørensen (s170423). " +
                    "It was made for the project 'Farm Management Information System " +
                    "incorporating Nitrogen Sensor' at DTU. " +
                    "Our supervisor was Ian Bridgwood",
                });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(
                    "/swagger/v1/swagger.json",
                    swaggerName + " " + swaggerVersion);
                c.RoutePrefix = string.Empty;
            }
            );

            app.UseRouting();

            app.UseAuthorization();

            app.UseCors("OriginPolicy");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
