# FMIS Manager

This is a FMIS Manager Service.

## How to run
Either you can reach the production environment with a local FMISManager (through all the run configs)
Or use docker-compose to build a local mongo-databse, and point at that with a local FMISManager.

There is also instructions beneath on how to deploy on google cloud. 

## How to local build Windows / linux / mac
- docker build -t fmis-manager .
- docker run --rm -it -p 80:80 -e ASPNETCORE_ENVIRONMENT=Production fmis-manager
- Are you runnning mongodb in docker aswell, try using the docker-compose instead, or make a network in docker for both.

## How to deploy on Google Cloud Run

### Cloud run init 
- Make sure you have Storage Object Admin & Stoage Object Creator roles in google cloud: https://console.cloud.google.com/iam-admin/iam?project=nitrogen-sensor-fmis
- Download glcoud: https://cloud.google.com/sdk/docs/install
- Run 'gcloud init', and follow steps
- Run 'gcloud auth configure-docker'

### After first run
- Create the docker image: 
	docker build -f Dockerfile -t gcr.io/nitrogen-sensor-fmis/fmis-manager .

- Push the docker image with the tag: 
	docker push gcr.io/nitrogen-sensor-fmis/fmis-manager

** BE AWARE THAT A MONGODB INSTANCE IS NEEDED FOR THIS SERVICE TO WORK! **
- The standard build is pointing at the production environment, use docker-compose to run it all locally.

- Run gcloud deploy: 
	gcloud run deploy fmis-manager --project nitrogen-sensor-fmis --image gcr.io/nitrogen-sensor-fmis/fmis-manager --allow-unauthenticated --port 80 --cpu 1 --memory 1024Mi --concurrency 80 --timeout 300 --region europe-west6

- Now your instance is up and running


## Mongodb in a Compute Engine
You do not need gcloud to make a compute engine.

### On google cloud platform
- Go to Compute Engine Tab
- Create new instance
	- Select 
		Allow HTTP traffic
		Allow HTTPS traffic
- Note the external ip to your vm instance, you need this to connect to your vm, or reach the db

- Go to VPC network
- Go to Firewall
- Check if the port you need to be open is open or add new firewall for the port


### Connect via ssh to the vm (or via google cloud platform's interface)
- Install docker (http://andrewcmaxwell.com/2016/07/how-to-setup-and-install-docker-on-google-compute-engine)
- Install docker compose (https://docs.docker.com/compose/install/)

Open up the connection to the outside world
- Run 'sysctl net.ipv4.conf.all.forwarding=1'
- Run 'sudo iptables -P FORWARD ACCEPT'

- Create a file on the server with ex. touch and nano commands "docker-compose.yml"
	- You can get inspiration for what is needed in docker-compose.yml

- run 'docker-compose up -d' to start the services
- Now you can reach the mongodb, and mongodb-express instances on the external ip mentioned earlier.

To destroy the running container again, use 'docker-compose down' 